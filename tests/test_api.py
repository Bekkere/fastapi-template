from starlette.testclient import TestClient
from api.main import app
from api.models.users import Users
from api.utils.hash import password_hash
from api.database import mongo as db

client = TestClient(app)


def test_db():
    users = db.users_db.find()
    for user in users:
        db.users_db.delete_one(user)
    admin = Users(
        full_name='Administrator',
        username='admin',
        email='admin@api.com',
        hash=password_hash('admin'),
        avatar=''
    )
    if db.users_db.find_one({'username': 'admin'}) is None:
        db.users_db.insert_one(admin.dict())
        db.client.close()


def test_root():
    response = client.get('/')
    assert response.status_code == 200
    assert response.json() == {'name': 'api', 'version': '0.1'}


def test_root_register_failed_user():
    response = client.post(
        '/register',
        json={
            "full_name": "Administrator",
            "username": "admin",
            "email": "admin@local",
            "password": "admin"
        }
    )
    assert response.status_code == 200
    assert response.json() == {
        "status": "failed",
        "reason": "user already exists",
        "username": "admin"
    }


def test_root_register_failed_email():
    response = client.post(
        '/register',
        json={
            "full_name": "Administrator",
            "username": "adminn",
            "email": "admin@local",
            "password": "admin"
        }
    )
    assert response.status_code == 200
    assert response.json() == {
        "status": "failed",
        "reason": "invalid email address",
        "username": "adminn"
    }


def test_root_register_success():
    response = client.post(
        '/register',
        json={
            "full_name": "Test",
            "username": "test",
            "email": "test@devx.co.id",
            "password": "test"
        }
    )
    assert response.status_code == 200
    assert response.json() == {
        "status": "success",
        "username": "test"
    }


def test_root_login_failed():
    response = client.post(
        '/token',
        data={
            "username": "failed",
            "password": "failed"
        }
    )
    assert response.status_code == 200
    assert response.json() == {
        "status": "failed",
        "reason": "user does not exists or invalid password",
        "username": "failed"
    }


def test_root_login_success():
    response = client.post(
        '/token',
        data={
            "username": "admin",
            "password": "admin"
        }
    )
    assert response.status_code == 200
    assert response.json()['token_type'] == 'bearer'


def test_users_failed():
    response = client.get('/users')
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_users_failed_token_invalid():
    response = client.get(
        '/users',
        headers={'Authorization': 'Bearer ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
                                  '.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJhZG1pbiIsImV4cCI6MTU2MzkzMjkwNX0._-1'
                                  '-6cMA8Ln6e1qCixcG7PNU4CXTYxaxdjNcYD0JAKI'}
    )
    assert response.status_code == 200
    assert response.json() == {
        "status": "failed",
        "reason": "token expired",
        "token": "ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
                 ".eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJhZG1pbiIsImV4cCI6MTU2MzkzMjkwNX0._-1"
                 "-6cMA8Ln6e1qCixcG7PNU4CXTYxaxdjNcYD0JAKI"}


def test_users_success():
    token = client.post(
        '/token',
        data={
            "username": "admin",
            "password": "admin"
        }
    ).json()['access_token']
    response = client.get(
        '/users',
        headers={'Authorization': 'Bearer ' + token}

    )
    assert response.status_code == 200
    assert response.json() == {"full_name": "Demo",
                               "username": "demo",
                               "email": "demo",
                               "hash": "demo",
                               "avatar": "demo"
                               }


def test_users_edit_failed():
    response = client.post('/users/edit')
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_users_edit_failed_token_invalid():
    response = client.post(
        '/users/edit',
        headers={
            'Authorization': 'Bearer ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
                             '.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJhZG1pbiIsImV4cCI6MTU2MzkzMjkwNX0._-1'
                             '-6cMA8Ln6e1qCixcG7PNU4CXTYxaxdjNcYD0JAKI'}

    )
    assert response.status_code == 200
    assert response.json() == {"status": "failed",
                               "reason": "token expired",
                               "token": "ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
                                        ".eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJhZG1pbiIsImV4cCI6MTU2MzkzMjkwNX0"
                                        "._-1-6cMA8Ln6e1qCixcG7PNU4CXTYxaxdjNcYD0JAKI"}


def test_users_edit_success():
    token = client.post(
        '/token',
        data={
            "username": "admin",
            "password": "admin"
        }
    ).json()['access_token']
    response = client.post(
        '/users/edit',
        headers={
            'Authorization': 'Bearer ' + token
        }
    )
    assert response.status_code == 200
    assert response.json() == {"full_name": "Demo",
                               "username": "demo",
                               "email": "demo",
                               "hash": "demo",
                               "avatar": "demo"
                               }
