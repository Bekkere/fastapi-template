import os
from pymongo import MongoClient

host = os.environ.get('API_DB_HOST', 'localhost')

client = MongoClient(host=host)
api_collection = client.api
users_db = api_collection.api_users
token_db = api_collection.api_token
