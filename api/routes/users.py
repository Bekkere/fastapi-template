from fastapi import APIRouter, Depends
from jwt import PyJWTError
from ..models.response import TokenExpired
from ..models.users import Users
from ..utils.oauth2 import oauth2_scheme
from ..utils.token import verify_token


router = APIRouter()


@router.get("/")
async def read_users(token: str = Depends(oauth2_scheme)):
    try:
        verify_token(token)
        return Users(
            full_name='Demo',
            username='demo',
            email='demo',
            hash='demo',
            avatar='demo'
        )
    except PyJWTError:
        return TokenExpired(
            status='failed',
            reason='token expired',
            token=token
        )


@router.post("/edit")
async def edit_users(token: str = Depends(oauth2_scheme)):
    try:
        verify_token(token)
        return Users(
            full_name='Demo',
            username='demo',
            email='demo',
            hash='demo',
            avatar='demo'
        )
    except PyJWTError:
        return TokenExpired(
            status='failed',
            reason='token expired',
            token=token
        )
