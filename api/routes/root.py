import datetime
from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from ..database import mongo as db
from ..models.response import UserError
from ..models.users import Register, Token, Users
from ..utils.email import check_email
from ..utils.hash import password_hash
from ..utils.token import create_token
from ..utils.users import check_user, verify_user


router = APIRouter()


@router.get("/")
async def api_home():
    return {
        'name': 'api',
        'version': '0.1'
    }


@router.post("/register")
async def register(form: Register):
    if check_user(form.username):
        return UserError(
            status='failed',
            reason='user already exists',
            username=form.username
        )
    elif not check_email(form.email):
        return UserError(
            status='failed',
            reason='invalid email address',
            username=form.username
        )
    else:
        user = Users(
            full_name=form.full_name,
            username=form.username,
            email=form.email,
            hash=password_hash(form.password),
            avatar=''
        )
        db.users_db.insert_one(user.dict())
        db.client.close()
        return {'status': 'success', 'username': user.username}


@router.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    if not verify_user(form_data.username, password_hash(form_data.password)):
        return UserError(
            status='failed',
            reason='user does not exists or invalid password',
            username=form_data.username
        )
    else:
        now = datetime.datetime.utcnow()
        expiry = now + datetime.timedelta(days=3)
        data = {
            'username': form_data.username,
            'password': form_data.password,
            'exp': expiry
        }
        token = create_token(data)
        return Token(access_token=token, token_type='bearer')
