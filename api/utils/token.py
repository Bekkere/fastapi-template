import jwt
from jwt.exceptions import ExpiredSignatureError


SECRET_KEY = "3ffb83904f32d22041df4f6a19984106c9627ea646a5b5aa5b22296c55e8fd54"
ALGORITHM = "HS256"


def create_token(data):
    return jwt.encode(data, key=SECRET_KEY, algorithm=ALGORITHM)


def verify_token(token):
    try:
        return jwt.decode(token, key=SECRET_KEY, algorithms=ALGORITHM)
    except ExpiredSignatureError:
        return False
