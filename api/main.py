from __future__ import absolute_import
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
import uvicorn
from api.database import mongo as db
from api.models.users import Users
from api.routes import root, users
from api.utils.hash import password_hash

app = FastAPI()

app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=["*"], allow_headers=["*"])
app.include_router(root.router, prefix='', tags=['root'])
app.include_router(users.router, prefix='/users', tags=['users'])


if __name__ == "__main__":
    admin = Users(
        full_name='Administrator',
        username='admin',
        email='admin@api.com',
        hash=password_hash('admin'),
        avatar=''
    )
    if db.users_db.find_one({'username': 'admin'}) is None:
        db.users_db.insert_one(admin.dict())
    else:
        print('User admin already exists')
    uvicorn.run(app, host='0.0.0.0', port=8000)
