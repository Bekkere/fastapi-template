# FastAPI Template

- Pipeline: [![pipeline status](https://gitlab.com/tdtimur/fastapi-template/badges/master/pipeline.svg)](https://gitlab.com/tdtimur/fastapi-template/commits/master)
- Coverage: [![coverage report](https://gitlab.com/tdtimur/fastapi-template/badges/master/coverage.svg)](https://gitlab.com/tdtimur/fastapi-template/commits/master)

This is a template for bootstrapping FastAPI project.
If you want to develop a RESTful web service using Python,
FastAPI is a reasonable choice for its simplicity yet powerful
features. With FastAPI, developing a fast, robust, 
and secure API for your applications made easy.

Yet here I try to make it even easier for you i.e. giving
a bootstrap and ready-to-develop code with following features:

- Based on FastAPI (obviously)
- Structured into (sub)modules for easy large projects
- Using MongoDB (more in future)
- Easy to add custom data models
- Typing with Pydantic
- Deployment using Uvicorn
- Token-based authentication using PyJWT
- Testing using Pytest
- Code coverage report using pytest-cov